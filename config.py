#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################### CONFIGURATION ###############################################
#                                                                                               #
wx_lat      	        = '5215.01N'                     	# coordinates APRS format           #
wx_lon      	        = '02055.58E'                    	# coordinates APRS format           #
wx_icon_table           = '/'                               # / - primary \ - secondary         #
wx_icon_symbol          = '_'                                                                   #
wx_comment  	        = 'WXDomoPy'      	                # beacon comment		            #
wx_err_comment 	        = 'No WX data'				        # comment when no data avaiable	    #
json_ip                 = '10.9.48.3'                       # domoticz IP adress                #
# required                                                                                      #
json_wind_direction_idx = '213'                             # wind direction sensor IDX         #
json_wind_speed_idx     = '213'                             # wind speed sensor IDX             #
json_wind_gust_idx      = '213'                             # wind speed gust IDX               #
json_temp_idx           = '211'                             # Temp sensor IDX                   #
# optionally                                                                                    #
json_rain_1h_idx        = '0'                                                                   #
json_rain_24h_idx       = '0'                                                                   #
json_rain_midnight_idx  = '0'                                                                   #
json_humi_idx           = '211'                             # Humidity sensor IDX               #
json_baro_idx           = '211'                             # Baromether  IDX                   #
# additional                                                                                    #
json_tempi_idx          = '0'                               # inside temperature                #
json_pm_1_idx           = '0'                               # PM 1.0 sensor IDX                 #
json_pm_25_idx          = '140'                             # PM 2.5 sensor IDX                 #
json_pm_10_idx          = '141'                             # PM 10 sensor IDX                  #
json_general_pm_idx     = '0'                               # General PM sensor                 #
json_voltage_batt_idx   = '0'                               # Battery voltage in comment        #
json_voltage_batti_idx  = '98'                              # Battery voltage in frame          #
# extra addons                                                                                  #
json_thunder_dist_idx    = '0'                              # lightning detector distance       #
json_thunder_enrg_idx    = '0'                              # lightning detector energy         #
# comments                                                                                      #
#                                                                                               #
# Solar Flux addon                                                                              #
json_solar_flux_idx      = '309'                             # enabled, disabled SFI            #
######################## DO NOTE EDIT BELLOW THIS LINE ##########################################